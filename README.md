How to set up: 
launch:
npm install
mvn clean install

ready!
To start getting your toes wet in TDD, remember this steps:
Write a failing test
Write the minimum code to make your test pass
Refactor

Let's go!

Kata1:

Convert arabic numerals from 1 to 10 to Roman numerals ( I, II, III, IV, V, VI, VII, VIII, IX, X)

Kata2:

Write a program that prints one line for each number from 1 to 100
For multiples of three print Fizz instead of the number
For the multiples of five print Buzz instead of the number
For numbers which are multiples of both three and five print FizzBuzz instead of the number
