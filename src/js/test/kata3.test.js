const kata = require('../src/kata3.js');
const assert = require('assert');

describe('Kata 3 test suite', function() {
    before(function() {
        //executes once before all tests
    });

    beforeEach(function() {
        //executes once before each test
    });

    afterEach(function() {
        //executes once after each test
    });

    after(function() {
        //executes once after all test
    });

    it('should add 2 numbers', function() {
        assert.equal(kata(1,1), 2)
    });
});
