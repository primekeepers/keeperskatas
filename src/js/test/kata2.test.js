const {kata, kata2} = require('../src/kata2.js');
const assert = require('assert');
const chai = require('chai');

const expect = chai.expect,
    sinon = require('sinon'),
    sinonChai = require('sinon-chai');

chai.use(sinonChai);
/* Kata2:

Write a program that prints one line for each number from 1 to 100
For multiples of three print Fizz instead of the number
For the multiples of five print Buzz instead of the number
For numbers which are multiples of both three and five print FizzBuzz instead of the number
 */

describe('Kata 2 test suite', function() {
    /*it('should print 100 lines', function() {
        assert.equal(kata().length, 100);
    });

    it('should call console.log once', function() {
        sinon.spy(console, 'log');
        kata();
        expect(console.log).to.have.been.called;
    });

    it('should return Fizz in the third position', function() {
        assert.equal(kata()[2], 'Fizz');
    });

    it('should return Buzz in the fifth position', function() {
        assert.equal(kata()[4], 'Buzz');
    });

    it('should return FizzBuzz in the fifteen position', function() {
        assert.equal(kata()[14], 'FizzBuzz');
    });*/
    it('test performance 1', function() {
        var startTime = new Date();

        kata();

        var endTime = new Date();

        console.log(`Call to doSomething took ${endTime.getTime() - startTime.getTime()} milliseconds`);
    });

    it('test performance 2', function() {
        var startTime = new Date();

        kata2();

        var endTime = new Date();

        console.log(`Call to doSomething took ${endTime.getTime() - startTime.getTime()} milliseconds`)
    });
});
