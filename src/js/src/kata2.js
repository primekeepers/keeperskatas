/* Kata2:

Write a program that prints one line for each number from 1 to 100
For multiples of three print Fizz instead of the number
For the multiples of five print Buzz instead of the number
For numbers which are multiples of Both three and five print FizzBuzz instead of the number
 */
const multipleOf = (number) => {
    return function pepe(ofNumber) {
        return ofNumber % number === 0;
    }
}
const multipleOfThree = multipleOf(3);
const multipleOfFive = multipleOf(5);
const multipleOfBoth = multipleOf(15);
const fizz = 'Fizz';
const buzz = 'Buzz';
const fizzBuzz = 'FizzBuzz';
const result = Array.from({length: 1000000}, (_, i) => i + 1);
const resultsKeys = Object.keys(result);

function kata() {
    const res = [];
    for (let i = 1; i <= 1000000; i++) {
        if (multipleOfBoth(i)) {
            res.push(fizzBuzz);
        } else if (multipleOfThree(i)) {
            res.push(fizz);
        } else if (multipleOfFive(i)) {
            res.push(buzz);
        } else {
            res.push(i.toString());
        }

    }
    return res;
}

function kata2() {
    return resultsKeys.map(number => {
        if (multipleOfBoth(number)) {
            return fizzBuzz;
        } else if (multipleOfThree(number)) {
            return fizz;
        } else if (multipleOfFive(number)) {
            return buzz;
        } else {
            return number;
        }
    });
}
module.exports = {
    kata,
    kata2
}

